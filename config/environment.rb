# Load the rails application
require File.expand_path('../application', __FILE__)
require 'active_support/core_ext/numeric'

# Initialize the rails application
DepartureTimes::Application.initialize!
