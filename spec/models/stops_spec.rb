require 'rails_helper'

RSpec.describe Stops, :type => :model do
  before do
    WebMock.allow_net_connect!
    # only enable muni
    APP_CONFIG['supported_agencies'] = {'sf-muni' => true} 
  end
  
  it 'Should return all routes' do
    all_routes = Stops.get_all_routes(37.77570128281339, -122.40769386291504)
    all_routes.length.should > 0
    agency_obj = all_routes.first
    agency_tag, routes = agency_obj.first
    p agency_tag
    route = routes.first
    p route
    route.should have_key('tag')
    route.should have_key("title")
    route.should have_key("stop")
    stop = route["stop"].first
    stop.should have_key("tag")
    stop.should have_key("title")
    stop.should have_key("lat")
    stop.should have_key("lon")
  end
end
