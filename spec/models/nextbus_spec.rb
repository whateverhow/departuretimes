require 'rails_helper'

RSpec.describe Nextbus, :type => :model, :caching => true do
  before do
    WebMock.allow_net_connect!
  end
  
  it 'Should return agency list' do
    agency_list = Nextbus.agency_list
    agency_list.should be_kind_of(Array)
    agency_list.length.should > 0
  end
  
  it 'Should return predictions' do
    predictions = Nextbus.predictions('sf-muni', '14661')
    predictions.should have_key('direction')
    predictions['direction'].should have_key("title")
    predictions['direction'].should have_key("prediction")
  end
  
end
