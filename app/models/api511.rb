# Api 511 does not give stop geocode
# In this app, we are not using it.
class Api511
  class << self
    def test
      p 'api 511 test'
    end
    
    def getAgencies
      Rails.cache.fetch('GetAgencies', :expires_in => 7.days) do
        request_url = build_url("GetAgencies")
        response = HTTParty.get(request_url)
        response.parsed_response.try(:[], "RTT").try(:[], 'AgencyList').try(:[], 'Agency')
      end
    end
    
    def getRoutesForAgencies(agency_names)
      agency_names_str = agency_names.join('|');
      Rails.cache.fetch(agency_names_str, :expires_in => 7.days) do
        request_url = build_url("GetRoutesForAgencies", {:agencyNames => agency_names_str})
        response = HTTParty.get(request_url)
        response.parsed_response.try(:[], "RTT").try(:[], "AgencyList").try(:[], "Agency")
      end
    end
    
    def getRoutesForAgency(agency_name)
      request_url = build_url("GetRoutesForAgency", {:agencyName => agency_name})
      response = HTTParty.get(request_url)
      response.parsed_response
    end
    
    def getStopsForRoutes(agency_name, route)
      code = route.try(:[], "Code")
      route_direction = route.try(:[], "RouteDirectionList").try(:[], "RouteDirection")
      routeIDF = route_direction.each_with_object([]) { |direction, ret| ret.push( agency_name + '~' + code + '~' + direction.try(:[], "Code")) }
      request_url = build_url("GetStopsForRoutes", {:routeIDF => routeIDF.join("|")})
      p request_url
      response = HTTParty.get(request_url)
      response.parsed_response
    end
    
    def getNextDeparturesByStopName(stopName)
      
    end

    def getNextDeparturesByStopCode(stop_code)
      request_url = build_url("GetNextDeparturesByStopCode", {:stopCode => stop_code})
      response = HTTParty.get(request_url)
      response.parsed_response
    end
    
    private
    
    # build an url for 511 api request
    # Params:
    # - request_name: request name from http://511.org/docs/RTT%20API%20V2.0%20Reference.pdf table 1
    # - queries: optional. Additional params for query
    def build_url(request_name, queries = {})
      return APP_CONFIG["api511_base_url"] + request_name + APP_CONFIG["api511_url_ext"] + "?" + queries.merge({:token => APP_CONFIG["api_token"]}).to_query.gsub("%7E", "~")
    end
  end
end