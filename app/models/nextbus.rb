class Nextbus
  class << self
    def agency_list
      Rails.cache.fetch('nextbus_agency_list', :expires_in => 7.days) do
        response = HTTParty.get(build_url({:command => 'agencyList'}))
        response.parsed_response.try(:[], 'body').try(:[], 'agency')
      end
    end
    
    def route_list(agency_tag)
      Rails.cache.fetch('nextbus_' + agency_tag, :expires_in => 7.days) do
        response = HTTParty.get(build_url({:command => 'routeList', :a => agency_tag}))
        response.parsed_response.try(:[], 'body').try(:[], 'route')
      end
    end
    
    #Nextbus.route_config('actransit', 'B')
    def route_config(agency_tag, route_tag)
      Rails.cache.fetch('nextbus_config_' + agency_tag.to_s + route_tag.to_s, :expires_in => 7.days) do
        response = HTTParty.get(build_url({:command => 'routeConfig', :a => agency_tag, :r => route_tag}))
        response.parsed_response.try(:[], 'body').try(:[], 'route')
      end
    end
    
    # Get predictions from stop id
    # cache predictions for 1 minute
    def predictions(agency_tag, stop_id)
      Rails.cache.fetch('nextbus_prediction_' + agency_tag.to_s + stop_id.to_s, :expires_in => 60.seconds) do
        response = HTTParty.get(build_url({:command => 'predictions', :a => agency_tag, :stopId => stop_id}))
        res = response.parsed_response.try(:[], 'body').try(:[], 'predictions')
        if res
          return res.select{|x| x['direction']}
        end
        nil
      end
    end
    
    private
    
    def build_url(query)
      return APP_CONFIG["nextbug_base_url"] + '?' + query.to_query
    end
  end
end