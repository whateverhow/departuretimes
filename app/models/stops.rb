# Stops process the logic of selecting nearest stops
# and massage data
class Stops
  class << self

    # The main function to retrive all stop predictions
    # -param lat float, latitude
    # -param lon float, longitude
    def all_nearby_predictions(myLat, myLon)
      
      stops_by_stop_id = get_nearby_routes(myLat, myLon)
      stops = sort_stops(stops_by_stop_id).take(10) #maximum 10 stops
      stops = getPredictions(stops)
      flattenData(stops)
    end
    
    # Retrive all route configs
    def get_all_routes(lat, lon)
      agencies = Nextbus.agency_list
      all_routes = []
      agencies.select! { |a| APP_CONFIG['supported_agencies'].try(:[], a['tag']) === true }
      agencies.each do |agency|
        inside_bounding_rect = check_in_bounding_rect(lat, lon, agency['tag'])
        #p 'Inside bounding Rect: ' + agency['tag'] + ' - ' + inside_bounding_rect.to_s
        next unless inside_bounding_rect
        all_routes.push({agency['tag'] => Nextbus.route_list(agency.try(:[], "tag"))})
      end
      process_all_routes(all_routes)
    end
    
    def process_all_routes(all_routes)
      @bounding_rects = @bounding_rects || {}
      all_routes.map! do |agency_obj|
        agency_tag, routes = agency_obj.first
        bounding_rect = {:lat_min => nil, :lat_max => nil, :lon_min => nil, :lon_max => nil}
        agency_obj.try(:[], agency_tag).map do |route|
          if route.kind_of?(Hash)
            route_tag = route.try(:[], 'tag')
          else
            agency_obj = nil
            next
          end
          route_config = Nextbus.route_config(agency_tag, route_tag)
          stops = route_config.try(:[], 'stop')
          stops.map! do |stop|
            stop['lat'] = stop['lat'].to_f
            stop['lon'] = stop['lon'].to_f
            #p stop['lat'].to_s + ',' + stop['lon'].to_s
            unless @bounding_rects.has_key?('agency_bounding_rect_' + agency_tag)
              bounding_rect[:lat_min] = stop['lat'] if (bounding_rect[:lat_min].nil? || stop['lat'] < bounding_rect[:lat_min])
              bounding_rect[:lat_max] = stop['lat'] if (bounding_rect[:lat_max].nil? || stop['lat'] > bounding_rect[:lat_max])
              bounding_rect[:lon_min] = stop['lon'] if (bounding_rect[:lon_min].nil? || stop['lon'] < bounding_rect[:lon_min])
              bounding_rect[:lon_max] = stop['lon'] if (bounding_rect[:lon_max].nil? || stop['lon'] > bounding_rect[:lon_max])
            end 
            stop
          end
          route['stop'] = stops
          route
        end
        unless @bounding_rects.has_key?('agency_bounding_rect_' + agency_tag)
          set_agency_bounding_rect(agency_tag, bounding_rect)
        end
        agency_obj
      end
      all_routes
    end
    
    # Get a lat, lon bounding rect for an agency
    def get_agency_bounding_rect(agency_tag)
      @bounding_rects = @bounding_rects || {}
      return @bounding_rects['agency_bounding_rect_' + agency_tag] if @bounding_rects.has_key?('agency_bounding_rect_' + agency_tag)
      @bounding_rects['agency_bounding_rect_' + agency_tag] = Rails.cache.read('agency_bounding_rect_' + agency_tag.to_s)
      @bounding_rects['agency_bounding_rect_' + agency_tag]
    end
    
    # Check if a coordinate is in this agency bounding box
    def check_in_bounding_rect(lat, lon, agency_tag)
      rect  = get_agency_bounding_rect(agency_tag)
      return true unless rect #no cache yet
      radius_padding = APP_CONFIG['nearby_radius_max'].to_f
      return false unless rect[:lat_min] && rect[:lon_min] #cached but empty stops in agency
      return false if lat < rect[:lat_min] - radius_padding || lat > rect[:lat_max] + radius_padding
      return false if lon < rect[:lon_min] - radius_padding || lon > rect[:lon_max] + radius_padding
      true
    end
    
    # Set the agency bounding rect
    def set_agency_bounding_rect(agency_tag, bounding_rect)
      Rails.cache.write('agency_bounding_rect_' + agency_tag.to_s, bounding_rect, time_to_idle: 2.days, timeToLive: 30.days)
    end
    
    # Get nearby routes. With a radius set by in_radius()
    # return a hash with key using stop_id and values = stops object 
    # We can further use these stop_ids to retrive predictions
    # r = Stops.get_nearby_routes(37.809458, -122.268287)
    def get_nearby_routes(lat, lon)
      all_routes = get_all_routes(lat, lon)
      nearby_stops = {};
      all_routes.each do |agency_obj|
        next unless agency_obj
        agency_obj.each do |agency_tag, routes|
          routes.each do |route|
            route_name = route["title"]
            route_tag = route["tag"]
            route["stop"].each do |stop|
              stop_id = stop["stopId"]
              dist = get_distance(stop, {"lat" => lat, "lon" => lon})
              if !dist.nil? && (!nearby_stops.has_key?(stop_id) || dist < nearby_stops[stop_id][:distance])
                nearby_stops[stop_id] = {
                  :agency => agency_tag, 
                  :stop_id => stop["stopId"], 
                  :lat => stop['lat'], 
                  :lon => stop['lon'], 
                  :title => stop["title"], 
                  :distance => dist,
                  :routeTag => route_tag,
                  :routeTitle => route_name
                }
              end
            end
          end
        end
      end
      nearby_stops
    end
    
    # Get predictions for all stops returned by get_nearby_routes()
    # stops = Stops.get_nearby_routes(37.809458, -122.268287)
    # predictions = Stops.getPredictions(stops)
    def getPredictions(stops)
      stops.each do |stop|
        stop[:predictions] = Nextbus.predictions(stop[:agency], stop[:stop_id])
      end
      return stops.select { |s| s[:predictions] && s[:predictions].length > 0 }
    end
    
    # Sort Stops by distance
    def sort_stops(stops_by_stop_id)
      stops_by_stop_id.values.sort { |x, y| x[:distance] <=> y[:distance] } 
    end
    
    private
    
    # Get distance between 2 points
    # Since they are very very close, I use a very simple sqrt calculate to get approximately distance
    # 0.0159647157245799 * x(miles) = latlng dist
    def get_distance(stop1, stop2)
      d1 = stop1['lat'] - stop2['lat']
      d2 = stop1['lon'] - stop2['lon']
      if in_radius(d1) && in_radius(d2)
        dist = get_distance_raw(d1, d2)
        if in_radius(dist)
          return dist
        end
      end
      nil
    end
    
    # Get direct distance sqrt
    def get_distance_raw(lat_diff, lon_diff)
    	Math.sqrt(lat_diff * lat_diff + lon_diff * lon_diff)
    end
    
    #check if it's in a range. We use 0.5 miles hardcoded. But would ideal to put in the config
    # 0.5 (miles) * 0.0159647157245799 = 0.00798235786229
    def in_radius(dist)
      return dist < APP_CONFIG['nearby_radius_max'].to_f
    end
    
    # Flatten data
    # sample return
    # [{
    #   "agencyTitle": "AC Transit",
    #   "agencyTag": "actransit",
    #   "routeTitle": "12",
    #   "routeTag": "12",
    #   "stop_id": "57111",
    #   "stopTitle": "Broadway & Thomas L Berkley Way (19th St B",
    #   "directionTitle": "To Berkeley BART",
    #   "prediction": [1409890652, 1409892452, 1409894252]
    # },...]
    def flattenData(stops)
      ret = []
      # each route direction only show 1 stop
      # key = (route_tag + directionTitle) to track displayed stops and remove duplicate stops
      route_direction_stops = {}
      stops.map do |stop|
        predictions = stop[:predictions]
        unless predictions.kind_of?(Array)
          predictions = [predictions]
        end
        predictions.each do |prediction|
          next unless prediction
          direction = prediction.try(:[], 'direction')
          p route_direction_stops
          next unless direction.kind_of?(Hash)
          
          tmp_ret = {
            :agencyTitle => prediction.try(:[], 'agencyTitle'),
            :agencyTag => stop.try(:[], :agency),
            :routeTitle => prediction.try(:[], 'routeTitle'),
            :routeTag => prediction.try(:[], 'routeTag'),
            :stop_id => stop.try(:[], :stop_id),
            :stopTitle => prediction.try(:[], 'stopTitle'),
            :directionTitle => direction.try(:[], 'title')
          }
          
          next unless tmp_ret[:directionTitle]
          tmp_ret[:routeTitle] ||= stop[:routeTitle]
          tmp_ret[:stopTitle] ||= stop[:title]
          tmp_ret[:routeTag] ||= stop[:routeTag]
          tmp_ret[:prediction] = []
          tmp_ret[:lat] = stop[:lat]
          tmp_ret[:lon] = stop[:lon]
          #filter same route on different stops
          if route_direction_stops.has_key?(tmp_ret[:routeTag].to_s + tmp_ret[:directionTitle].to_s)
            next
          else
            route_direction_stops[tmp_ret[:routeTag].to_s + tmp_ret[:directionTitle].to_s] = true
          end
          predct_times = direction.try(:[], 'prediction')
          if predct_times.kind_of?(Array)
            predct_times.take(4).each do |p| #max 4 predictions
              tmp_ret[:prediction].push(p['epochTime'].to_i / 1000)
            end
          end
          ret.push(tmp_ret) unless tmp_ret[:prediction].length == 0
        end
      end
      ret
    end
  end
end