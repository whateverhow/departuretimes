class CreateStopGeocodeJob
  class << self
    # 511 not in use
    # Deprecated
    #def api511GetAllRoutes
    #  agencies = Api511.getAgencies;
    #  agencies_arr = agencies.each_with_object([]) { |agency_obj, ret| #ret.push(agency_obj.try(:[], 'Name')) }
    #  all_routes = Api511.getRoutesForAgencies(agencies_arr);
    #  all_routes
    #end
    
    #def api511GetStopsForRoute
    #  routes = getAllRoutes
    #  agency_name = route.try(:[], "Name")
    #  route_list = route.try(:[], "RouteList").try(:[], "Route")
    #  ret = Api511.getStopsForRoutes(agency_name, route_list.first)
    #  ret
    #end
    
    #Nextbus cache all routes
    def nextbusGetAllRoutes
      agencies = Nextbus.agency_list
      all_routes = []
      agencies.each do |agency|
        all_routes.push({agency['tag'] => Nextbus.route_list(agency.try(:[], "tag"))})
      end
      process_all_routes(all_routes)
    end
    
  end
end