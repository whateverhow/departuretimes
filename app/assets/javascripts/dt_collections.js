define([
  'underscore', 
  'backbone', 
  'dt_model'
  ], function(_, Backbone, dtModel){
    var dtCollection = Backbone.Collection.extend({
    // Reference to this collection's model.
    model: dtModel,
  
    url: '/dt/nearby_departures'
  });
  return new dtCollection;
});
