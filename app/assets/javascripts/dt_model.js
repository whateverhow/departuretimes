define([
'underscore',
'backbone',
'dt_collections'
], function(_, Backbone,links) {
  var dtModel = Backbone.Model.extend({
    // Default attributes for the todo.
    defaults: {},

    initialize: function() {
    },
	
    // Remove this link from *localStorage* and delete its view.
    clear: function() {
      this.destroy();
      this.view.remove();
    }
  });
  dtModel.prototype.parse = function(response) {
    var attr = response && _.clone(response) || {};
    if (attr.prediction && _.isArray(attr.prediction)) {
       attr.prediction = attr.prediction.join(',');
    } else {
      attr.prediction = [];
    }
    return attr;
  };
  return dtModel;
});


