define([
  'underscore',
  'backbone',
  'dt_collections',
  'gmap'
  ], function(_, Backbone, dt_collections){
  _.templateSettings = {
    interpolate: /\{\%\=(.+?)\%\}/g,
    evaluate: /\{\%(.+?)\%\}/g
  };
  var compiled_template = _.template($('#departure_cell').html());
  var dtView = Backbone.View.extend({
    //... is a list tag.
    tagName:  "div",
   
    initialize: function() {
      _.bindAll(this, 'render');
      //this.model.bind('change', this.render);
      //this.model.view = this;
    },
	
    // Render the content of a departure time
    render: function() {
      var data = this.model.toJSON();
      data.prediction = data.prediction.split(',');
      var now = (new Date()).getTime() / 1000;
      for (var i = 0; i < data.prediction.length; i++) {
        data.prediction[i] = data.prediction[i] - now;
        if (data.prediction[i] < -60 * 5) {
          data.prediction[i] = null;
        }
        else if (data.prediction[i] < 0) {
          data.prediction[i] = 'arriving';
        } else {
          data.prediction[i] = Math.ceil(data.prediction[i] / 60) + 'mins';
        }
      }
    	this.$el.html(compiled_template({ route: data }));
    	return this;
    }
  });
  return dtView;
});
