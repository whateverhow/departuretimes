/*task list*/
define([
  'underscore',
  'backbone',
  'dt_collections',
  'dt_view',
  'gmap'
  ], function(_, Backbone, dtCollections, dtView, gmap){
    var AppView = Backbone.View.extend({
      el: $('#dt-content'), // attaches `this.el` to an existing element.
      currentLocation: [37.809458, -122.268287],
      getLocation: function (cb) {
        var that = this;
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (location) {
            var latLng = location.coords;
            that.currentLocation[0] = latLng.latitude;
            that.currentLocation[1] = latLng.longitude;
            that.currentLocation[0]
            cb && cb();
          });
        } else {
          //console.log("Geolocation is not supported by this browser.");
          cb && cb();
        }
      },
    
      initialize: function () {
        var that = this;
        this.getLocation(function () {
          that.listenTo(dtCollections, 'reset', that.addAll);
          dtCollections.fetch({
            reset: true, 
            data: 'lat=' + that.currentLocation[0] + '&lon=' + that.currentLocation[1]
          });
          window.dtCollections = dtCollections;
          //display default address
          $('#address').val(that.currentLocation.join(','));
          //update google map center
          gmap.getGeoCode(that.currentLocation.join(','), function () {});
          
          that.bind();
        });
      },
      
      bind: function () {
        var that = this;
        $('#submit_location').on('click', function (e) {
          $('#dt-content').html('Loading');
          gmap.getGeoCode($('#address').val(), function (latLng) {
            that.currentLocation[0] = latLng.lat();
            that.currentLocation[1] = latLng.lng();
            dtCollections.fetch({
              reset: true,
              error: function (collection, response, options) {
                //console.log('error:', collection, response, options);
                $(that.el).html('Opps! Error! Please try again.');
              },
              data: 'lat=' + that.currentLocation[0] + '&lon=' + that.currentLocation[1]
            });
          });
        });
      },
      
      render: function () {
        
      },
      
      addOne: function (dt) {
        if (!this.stopIds[dt.get('stop_id')]) {
          this.points.push([dt.get('lat'), dt.get('lon')]);
          this.stopIds[dt.get('stop_id')] = this.points.length;
        }
        dt.set('iconNum', this.stopIds[dt.get('stop_id')]);
        var view = new dtView({model: dt});
        this.$el.append(view.render().el);
      },
      
      addAll: function () {
        $(this.el).html('');
        this.points = [];
        this.stopIds = {};
        
        if(dtCollections.length === 0) {
          $(this.el).html('Sorry, no departure time found near you. Please try a different location.');
        } else {
          dtCollections.each(this.addOne, this);
          gmap.setPoints(this.points);
        }
      }
    });
    return AppView;
});

