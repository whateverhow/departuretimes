define(['async!https://maps.googleapis.com/maps/api/js?key=AIzaSyAn4e14owFlBeUTcGqyQGDr-aDRt02c0jc'], function() {
  var Gmap = (function () {
    var map, markers = [], geoCoder;

    /**
     * Initialize google map, click events and markers
     */
    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(37.7831914, -122.4100396),
            zoom: 17,
            draggableCursor: 'crosshair'
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        geoCoder = new google.maps.Geocoder;
        var marker = new google.maps.Marker({
          position: null,
          map: null,
          icon: "http://maps.google.com/mapfiles/arrow.png",
        });
        markers.push(marker);
        google.maps.event.addListener(map, 'click', function(e) {
          lastClickedLatLng = e.latLng.lat() + ', ' + e.latLng.lng();
          document.getElementById('address').value = lastClickedLatLng;
          if (markers[0]) {
            markers[0].setPosition(e.latLng);
            markers[0].setMap(map);
          }
        });
    }
    initialize();

    /**
     * Convert an array to google LatLng object
     * example input: [37.963418076862744, -122.5568675994873]
     */
    function arr2LatLng(latlng) {
        return new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
    }

    /**
     * Clear all markers in map
     */
    function clearMarkers() {
        markers.forEach(function(marker, i) {
            if (i > 0) marker.setMap(null);
        });
    }

    /**
     * Return public methods
     */
    return {
      /**
       * Place/move a marker on map
       */
      setPoints: function (points) {
        if (points) {
          clearMarkers();
          points.forEach(function (p, i) {
            //set a marker
            if (markers[i + 1]) {
              markers[i + 1].setPosition(arr2LatLng(p));
              markers[i + 1].setMap(map);
            } else {
              var marker = new google.maps.Marker({
                position: arr2LatLng(p),
                map: map,
                icon: "http://maps.google.com/mapfiles/kml/paddle/" + (i + 1) + ".png",
              });
              markers.push(marker);
            }
          });
            
        }
      },
      
      getGeoCode: function (address, cb) {
        geoCoder.geocode({'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var p = results[0].geometry.location;
            map.setCenter(p);
            if (markers[0]) {
              markers[0].setPosition(p);
              markers[0].setMap(map);
            }
            cb && cb(p);
          } else {
            console.log("Geocode was not successful for the following reason: " + status);
          }
        });
      }
    };
  })();
  
  return Gmap;
});
