class Api511Controller < ApplicationController
  def getAgencies
    agencies = Api511.getAgencies
    return render json: agencies
  end
end
