### What is this repository for? ###

This is for the uber take home test: Departure Times
original link https://github.com/uber/coding-challenge-tools
Hosted at: http://departuretimes.herokuapp.com/

### Features ###

* Supports all agencies in Nextbus XML feed. For detailed list, see [config.yml - supported_agencies](https://bitbucket.org/whateverhow/departuretimes/src/b677b27fdd47d7901c9fb773efd27ea08b79473e/config/config.yml?at=master#cl-3)
* Use location data from browser/phone or LEFT CLICK on map to set current location. 
* View stops on map and corresponding departure times on left rail

#### Screenshot ####
![screen shot](https://bytebucket.org/whateverhow/departuretimes/raw/b677b27fdd47d7901c9fb773efd27ea08b79473e/doc/screenshot.jpg)

### How do I get set up? ###

* Back-end: Ruby on Rails, memcache, no database
* Departure prediction data coming from [Nextbus XML feed](http://www.nextbus.com/xmlFeedDocs/NextBusXMLFeed.pdf)
* Front-end: Backbone, underscore, requirejs, zepto(like jquery)


### Tech details###
* **Ruby on Rails** I have 6 months industry experience with Ruby on Rails. The backend cache some results for faster access. It also filter the data and search for nearby stops.
* **Memcached**: For both cache and database. It caches the route configs as they do not change frequently. Cache the predictions for 1 minute. The backend will search for all nearby stops from cached data based on current location. Comparing with using database, the memcache linear search might be slower than database search, but memcache is faster in retrieving data as well as more flexible in filtering data.
* **requirejs**: Probably not "required", but this gives further flexibility to expand the app to multiple pages. It's the first time I configure it with rails which took me some efforts.
* **Backbone.js**:  This is my second project using backbone. The first time was also a small scale app. Backbone was mainly being used for rendering the departure list on the left rail.


###Algorithms###
* Store agency list and agency route config into memcache
* Calculate a bounding box of each agent. The bounding box is represented by latitude min/max, and longitude min/max.
* If your current position is not inside current agent's bounding box, skip this agent.
* Search for all stops by filtered agent and find all stops near you in certain range, sort by distance
* Retrieve predictions by each stop id, then cache it for 1 minute
* Filter stop predictions. Only show the nearest stop prediction per route per direction.

### Trade-offs ###

* UI is not well polished
* Need more tests
* Does not automatically update predictions on front-end
* Documentation is not well written
* Only a few agencies in north California was enabled on hosted free herokuapp due to memcached server is limited to 25MB.. 
* Note: Enable all agencies does not sacrifice speed but requires larger memcache memory.
* Some direction data is "incorrectly" formatted, currently I just ignored "bad" data.

### How to run tests ###
```
bundle exec rspec
```
Currently, the specs are not fully covered. Only have a few examples

### How to install from repo ###
Make sure you have ruby 2.1.0 installed
Make sure you have local memcache server installed
```
git clone git@bitbucket.org:whateverhow/departuretimes.git
bundle install
bundle exec rails s
```
Then open http://localhost:3000

### Files to look at ###

Controller:

* app/controllers/dt_controller.rb

Models:

* app/models/nextbus.rb
* app/models/stops.rb


Specs

* spec/models/nextbus_spec.rb
* spec/models/stops.rb

Javascripts:

* app/assets/javascripts/app.js
* app/assets/javascripts/dt_collections.js
* app/assets/javascripts/dt_model.js
* app/assets/javascripts/dt_view.js
* app/assets/javascripts/gmap.js

### Author ###
Hao Wang